package main.java.com.skillbox.StudentsJournal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudentsJournalApplication {

	public static void main(String[] args) {
		SpringApplication.run(StudentsJournalApplication.class, args);
	}

}
