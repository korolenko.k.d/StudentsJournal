/**
 * Class description goes here (optional)
 *
 * @author: Kirill Korolenko [korolenko.k.d@gmail.com]
 * Date: 2021-02-18
 * Time: 15:59
 */

package com.skillbox.StudentsJournal.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class AvgScore {

  private final Integer studentId;
  private final Double avgScore;

  @JsonCreator
  public AvgScore(@JsonProperty("student_id") Integer studentId, @JsonProperty("average_score") Double avgScore) {
    this.studentId = studentId;
    this.avgScore = avgScore;
  }

  public Integer getStudentId() {
    return studentId;
  }

  public Double getAvgScore() {
    return avgScore;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    AvgScore avgScore1 = (AvgScore) o;
    return Objects.equals(studentId, avgScore1.studentId) &&
        Objects.equals(avgScore, avgScore1.avgScore);
  }

  @Override
  public int hashCode() {
    return Objects.hash(studentId, avgScore);
  }
}
